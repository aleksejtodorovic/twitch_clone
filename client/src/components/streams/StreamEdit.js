import _ from "lodash";
import React from "react";
import { connect } from "react-redux";
import { editStream } from "../../actions/stream_actions";
import { fetchStream } from "../../actions/stream_actions";
import StreamForm from "./StreamForm";

class StreamEdit extends React.Component {
  componentDidMount() {
    this.props.fetchStream(this.props.match.params.id);
  }

  onSubmit = formValues => {
    const { id } = this.props.match.params;

    this.props.editStream(id, formValues);
  };

  render() {
    if (!this.props.stream) {
      return <div>Loading...</div>;
    }

    return (
      <div className="ui segment">
        <h3>Edit a Stream</h3>
        <StreamForm
          initialValues={_.pick(this.props.stream, "title", "description")}
          onSubmit={this.onSubmit}
        />
      </div>
    );
  }
}

const mapStateToProps = ({ streams, auth }, { match }) => ({
  stream: streams[match.params.id],
  currentUserId: auth.userId
});

export default connect(
  mapStateToProps,
  { editStream, fetchStream }
)(StreamEdit);
