import React from "react";
import { Field, reduxForm } from "redux-form";

class StreamForm extends React.Component {
  renderError({ touched, error, warning }) {
    if (touched && error) {
      return (
        <div className="ui error message">
          <div className="header">{error}</div>
        </div>
      );
    }

    if (touched && warning) {
      return (
        <div className="ui warning message">
          <p>{warning}</p>
        </div>
      );
    }
  }

  renderInput = ({ input, label, type, meta }) => {
    const className = `field ${meta.error && meta.touched ? "error" : ""}`;

    return (
      <div className={className}>
        <label>{label}</label>
        <div>
          <input {...input} placeholder={label} type={type} />
          {this.renderError(meta)}
        </div>
      </div>
    );
  };

  onSubmit = formValues => {
    this.props.onSubmit(formValues);
  };

  render() {
    const { handleSubmit, pristine, reset, submitting } = this.props;

    return (
      <form className="ui form error" onSubmit={handleSubmit(this.onSubmit)}>
        <Field
          name="title"
          type="text"
          component={this.renderInput}
          label="Enter Title"
        />
        <Field
          name="description"
          type="text"
          component={this.renderInput}
          label="Enter Description"
        />

        <div>
          <button
            className="ui button primary"
            type="submit"
            disabled={submitting}
          >
            Submit
          </button>
          <button
            className="ui button"
            type="button"
            disabled={pristine || submitting}
            onClick={reset}
          >
            Clear Values
          </button>
        </div>
      </form>
    );
  }
}

const validate = ({ title, description }) => {
  const errors = {};

  if (!title) {
    errors.title = "You must enter a title.";
  }

  if (!description) {
    errors.description = "You must enter a description.";
  }

  return errors;
};

export default reduxForm({
  form: "stream-form",
  validate
})(StreamForm);
